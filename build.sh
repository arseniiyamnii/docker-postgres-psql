docker build -t psql.alpine - < ./Dockerfile.psql
docker build -t postgres.alpine - < ./Dockerfile.postgres
docker tag psql.alpine:latest 94.190.26.105:5000/my.psql
docker tag postgres.alpine:latest 94.190.26.105:5000/my.postgres
docker push 94.190.26.105:5000/my.postgres
docker push 94.190.26.105:5000/my.psql
